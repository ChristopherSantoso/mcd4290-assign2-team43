// cm to inch
function Inches() {
var cmToInchRef = document.getElementById("cmToInchValue")
var cmToInch = Number(cmToInchRef.value)
 cmToInchResult.innerHTML = (cmToInch*2.54).toFixed(2)
}

// inch to cm
function Centimetres() {
var inchToCMRef = document.getElementById("InchToCMValue")
var inchToCM = Number(inchToCMRef.value)
 InchToCMResult.innerHTML = (inchToCM/2.54).toFixed(2)
}

// pounds to kg
function Kilogram() {
var poundsToKilogramRef = document.getElementById("poundsToKilogramValue")
var poundsToKilogram = Number(poundsToKilogramRef.value)
 poundsToKilogramResult.innerHTML = (poundsToKilogram*0.45359).toFixed(2)
}

// kg to pounds
function Pounds() {
var kilogramToPoundsRef = document.getElementById("kilogramToPoundsValue")
var kilogramToPounds = Number(kilogramToPoundsRef.value)
 kilogramToPoundsResult.innerHTML = (kilogramToPounds/0.45359).toFixed(2)
}